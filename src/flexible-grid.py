"""
Grid MC exchange program.
"""
#################################################################################
#    Copyright (C) 2019 Martin Leandro Paleico                                  #
#    contact: name dot surname at uni dash goettingen dot de                    #
#                                                                               #
#    This program is free software: you can redistribute it and/or modify       #
#    it under the terms of the GNU General Public License as published by       #
#    the Free Software Foundation, either version 3 of the License, or          #
#    (at your option) any later version.                                        #
#                                                                               #
#    This program is distributed in the hope that it will be useful,            #
#    but WITHOUT ANY WARRANTY; without even the implied warranty of             #
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              #
#    GNU General Public License for more details.                               #
#                                                                               #
#    You should have received a copy of the GNU General Public License          #
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.     #
#################################################################################

from random import seed, uniform, sample
from math import sqrt, e, ceil, floor
from lammps import lammps
import numpy as np

seed(548948445)

class grid_point:
    def __init__(self, dead_zone=1.0, spring_constant=0.1, equil_distance=1.0):
        self.pos=[]
        self.gradient=[]
        self.gradient_unitary=[]
        self.e=0.0
        self.neigh=[]
        self.neigh_num=0
        self.neigh_distances=[]
        self.occupation=0 #occupied and fixed, unoccupied and free to move
        self.sim_id=-1
        self.cell=[-1, -1, -1]

        self.dz=dead_zone
        self.k=spring_constant
        self.req=equil_distance

    def set_neigh(self, new_neigh):
        self.neigh.append(new_neigh)
        self.reset_neigh_distances()
        self.neigh_num+=1

    def set_neigh_distances(self, new_distance, nid):
        if len(self.neigh_distances) != len(self.neigh):
            print("Wrong number of distances for neighbors: {} distances, {} neighbors".format(len(self.neigh_distances), len(self.neigh)))
            exit()

        if nid in self.neigh: #since we limit the number of neighbors, they are not all neighbors of each other
            self.neigh_distances[self.neigh.index(nid)]=new_distance
        
    def get_neigh(self, num):
        return self.neigh[num]

    def get_neigh_distances(self, nid):
        if len(self.neigh_distances) != len(self.neigh):
            print("Wrong number of distances for neighbors: {} distances, {} neighbors".format(len(self.neigh_distances), len(self.neigh)))
            exit()
        return self.neigh_distances[self.neigh.index(nid)]

    def get_all_neigh(self):
        return self.neigh

    def get_all_neigh_distances(self):
        if len(self.neigh_distances) != len(self.neigh):
            print("Wrong number of distances for neighbors: {} distances, {} neighbors".format(len(self.neigh_distances), len(self.neigh)))
            exit()
        return self.neigh_distances

    def del_neigh(self):
        pass

    def del_all_neigh(self):
        self.neigh=[]

    def del_all_neigh_distances(self):
        self.neigh_distances=[]

    def reset_neigh_distances(self,):
        self.neigh_distances=[0.0 for i in range(0, len(self.neigh))]
        if len(self.neigh_distances) != len(self.neigh):
            print("Wrong number of distances for neighbors: {} distances, {} neighbors".format(len(self.neigh_distances), len(self.neigh)))
            exit()

    def move(self, new_pos, pbc):
        if pbc is None:
            self.pos=np.array(new_pos)
        else:
            self.pos=np.array([xi-floor(xi/xsize)*xsize for xi, xsize in zip(new_pos, pbc)])

    def set_gradient(self, gradient):
        self.gradient=gradient
        norm=get_norm(gradient)
        if norm<0.00001:
            self.gradient_unitary=[0.0, 0.0, 0.0]
        else:
            self.gradient_unitary=get_unitary(gradient)

    def set_energy(self, energy):
        self.e=energy

    def flip(self):
        if self.occupation==0:
            self.occupation=1
        elif self.occupation==1:
            self.occupation=0
        else:
            print("ERROR: TRYING TO FLIP FIXED POINT")
            exit()

    def fix(self): #fixed points never get exchanged, so the grid doesn't drift in space
        self.occupation=2

    def set_sim_id(self, nid):
        self.sim_id=nid

    def set_cell(self, i, j, k):
        self.cell=[i, j, k]

def get_distance(v1, v2, pbc):
    if pbc is None:
        #distance=sqrt(sum([(v-u)**2 for v, u in zip(v1, v2)]))
        distance=np.linalg.norm(v2-v1)
    else:
        #diff_vec=get_difference_vector(v1, v2, pbc)
        #distance=sqrt(sum([dv**2 for dv in diff_vec]))
        #npv1=np.array(v1)
        #npv2=np.array(v2)
        distance=np.linalg.norm((v2-v1)-np.rint((v2-v1)/pbc)*pbc)
        #print(v1, v2, pbc, distance)
    return distance

def get_difference_vector(v1, v2, pbc):
    if pbc is None:
        #ret_vec=[r2i-r1i for r2i, r1i in zip(v1, v2)]
        ret_vec=v2-v1
    else:
        #ret_vec=[r2i-r1i for r2i, r1i in zip(v1, v2)]
        #ret_vec=[dx-int(round(dx/xsize))*xsize for dx, xsize in zip(ret_vec, pbc)]
        #npv1=np.array(v1)
        #npv2=np.array(v2)
        ret_vec=(v1-v2)-np.rint((v1-v2)/pbc)*pbc
    return ret_vec

def get_norm(v1):
    #norm=sqrt(sum([v**2 for v in v1]))
    return np.linalg.norm(np.array(v1))

def get_unitary(v1):
    norm=get_norm(v1)
    #v1_norm=[vi/norm for vi in v1]
    v1_norm=np.array(v1)/norm
    return v1_norm

class grid:
    def __init__(self):
        self.points=[]
        
        self.potential=None

        self.dz =0.0
        self.k  =0.0
        self.req=0.0

        self.is_periodic=False
        self.pbc=None

        #optimize
        self.linmove=0.05
        self.minimization_e=1000000000000000.0
        self.tolerance=0.001

    def set_parameters(dead_zone, spring_constant, equil_distance):

        self.dz=dead_zone
        self.k =spring_constant
        self.req=equil_distance

    def set_potential(self, potential):
        self.potential=potential

    def set_pbc(self, pbc=[0.0, 0.0, 0.0]):
        self.is_periodic=True
        self.pbc=pbc

    def read_restart(self, restart_file_name):
        rfile=open(restart_file_name, mode="r")
        self.points=[]
        self.pbc=[]
        for line in rfile:
            spline=line.split()
            if "pbc" in line:
                self.pbc.append(float(spline[2]))
            elif "grid" in line:
                pos=[float(spl) for spl in spline[1:4]]
                occ=int(spline[4])
                new_point=grid_point(dead_zone=self.dz, spring_constant=self.k, equil_distance=self.req)
                new_point.move(pos, self.pbc)
                new_point.occupation=occ
                self.points.append(new_point)
            elif "iter_step" in line:
                nstep=int(spline[1])
        rfile.close()
        return nstep

    def start_grid_2D(self, distance=1.0, npoints=1):
        u=0.1*distance
        n=0

        if isinstance(npoints, int):
            npointsx= npointsy= npoints
        else: # a list
            npointsx, npointsy= npoints[0:2] #unpack

        for x in range(0, npointsx):
            for y in range(0, npointsy):
                n+=1
                new_point=grid_point(dead_zone=self.dz, spring_constant=self.k, equil_distance=self.req)
                pos=[x*distance+uniform(-u, u), y*distance+uniform(-u, u), 0.0]
                new_point.move(pos, self.pbc)
                self.points.append(new_point)

    def start_grid_1D(self, distance=1.0, npoints=1):
        u=0.1*distance
        n=0
        for x in range(0, npoints):
            n+=1
            new_point=grid_point(dead_zone=self.dz, spring_constant=self.k, equil_distance=self.req)
            pos=[x*distance+uniform(-u, u), 0.0, 0.0]
            new_point.move(pos, self.pbc)
            self.points.append(new_point)

    def start_grid_3D(self, distance=1.0, npoints=1):
        u=0.1*distance
        n=0

        if isinstance(npoints, int):
            npointsx= npointsy= npointsz= npoints
        else: # a list
            npointsx, npointsy, npointsz= npoints #unpack

        for z in range(0, npointsz):
            for x in range(0, npointsy):
                for y in range(0, npointsx):
                    n+=1
                    new_point=grid_point(dead_zone=self.dz, spring_constant=self.k, equil_distance=self.req)
                    pos=[x*distance+uniform(-u, u), y*distance+uniform(-u, u), z*distance+uniform(-u, u)]
                    new_point.move(pos, self.pbc)
                    self.points.append(new_point)

    def flip_points(self, n):
        chosen_points=sample(range(0, len(self.points)), n)
        for p in chosen_points:
            self.points[p].flip()

    def flip_points_in_region(self, n, center=[0.0, 0.0, 0.0], radius=1.0):
        chosen_points=sample(range(0, len(self.points)), len(self.points)) #get a random, unique reordering of all the points
        nchosen=0
        for p in chosen_points:
            pos=self.points[p].pos
            dist=get_distance(pos, center, None)
            if dist<radius:
                self.points[p].flip()
                nchosen+=1
            if nchosen==n:
                return
        if nchosen<n:
            print("FAILED TO FLIP ENOUGH POINTS, INCREASE SIZE OF REGION")
            exit()

    def fix_points(self, n):
        occupied=[]
        for i, p in enumerate(self.points):
            if p.occupation!=0:
                occupied.append(i)
        chosen=sample(occupied, n)
        for p in chosen:
            self.points[p].fix()

    def delete_unoccupied(self):
        occupied=[]
        for i, p in enumerate(self.points):
            if p.occupation!=0:
                occupied.append(i)
        temp=self.points
        self.points=[]
        for p in occupied:
            self.points.append(temp[p])

    def delete_neighbors(self):
        for p in self.points:
            p.delete_all_neigh()

    def assign_neighbors(self, max_neigh=4, cutoff=4.0, rcellcut=1.0):

        #cell list algorithm
        cell_size=[boxsize/ceil(boxsize/rcellcut) for boxsize in self.pbc] #conservative cell sizing
        cell_numbers=[int(bs/cs) for bs, cs in zip(self.pbc, cell_size)]
        #cells=[[[[],]*cell_numbers[2]]*cell_numbers[1]]*cell_numbers[0] #dirty, but works, allows us to index as cells[i][j][k] for x, y, z cells
        cells=[[[ [] for k in range(cell_numbers[2])] for j in range(cell_numbers[1])] for i in range(cell_numbers[0])]

        for n, p in enumerate(self.points):
            i, j, k=[int(pos/cs) for cs, pos in zip(cell_size, p.pos)]
            p.set_cell(i, j, k) #assign cell to atom
            cells[i][j][k].append(n) #assign atom to cell
        #when searching neighbor cells, careful when exceeding max cell number/position in array

        for n, p in enumerate(self.points):
            #find ids of neighboring atoms
            neigh_ids=[]
            for di in [-1, 0, 1]:
                for dj in [-1, 0, 1]:
                    for dk in [-1, 0, 1]:
                        i=p.cell[0]+di
                        if i==cell_numbers[0]: #avoid slicing beyond the array size
                            i=0
                        j=p.cell[1]+dj
                        if j==cell_numbers[1]: #avoid slicing beyond the array size
                            j=0
                        k=p.cell[2]+dk
                        if k==cell_numbers[2]: #avoid slicing beyond the array size
                            k=0
                        [neigh_ids.append(nid) for nid in cells[i][j][k]]
            #calculate distances and assign actual neighbors
            distances=[]
            for nid in neigh_ids:
                if nid!=n:
                    d=get_distance(p.pos, self.points[nid].pos, self.pbc)
                    if d<cutoff: distances.append([d, nid])
            distances.sort(key=lambda x: x[0])
            p.del_all_neigh()
            for i in range(0, min(max_neigh, len(distances))):
                p.set_neigh(distances[i][1])

        return

        """
        for n1, p1 in enumerate(self.points):
            distances=[]
            for n2, p2 in enumerate(self.points):
                if p2 != p1:
                    d=get_distance(p1.pos, p2.pos, self.pbc)
                    if d<cutoff: distances.append([d, n2])
            distances.sort(key=lambda x: x[0])
            p1.del_all_neigh()
            for i in range(0, min(max_neigh, len(distances))):
                p1.set_neigh(distances[i][1])
            #print(p1.get_all_neigh())
        """

    def optimize(self, nstep=-1, linmove=0.025):
        if nstep==1:
            self.linmove=linmove
        if self.linmove<0.00001:
            return True
        energy=0.0
        #get gradients
        for ownid, p in enumerate(self.points):
            neighbors=p.get_all_neigh()
            if len(neighbors)==0:
                print("ORPHANED POINT")
                exit()
            neighbor_positions=[self.points[n].pos for n in neighbors]
            self_position=p.pos

            for nid, npos in zip(neighbors, neighbor_positions): #calculate neighbor distances
                if abs(p.get_neigh_distances(nid))<0.000000001:
                    neighdist=get_distance(self_position, npos, self.pbc)
                    p.set_neigh_distances(neighdist, nid)
                    #print(nid, neighdist, ownid)
                    if ownid in self.points[nid].get_all_neigh():
                        self.points[nid].set_neigh_distances(neighdist, ownid) #set distance for neighbor too
                    
            #print(ownid, neighbors, p.neigh_distances)

            if p.occupation==0:
                eatom, gradient=self.get_gradient(self.potential, self_position, neighbor_positions, p.get_all_neigh_distances())
                eatom=0.5*eatom
                p.set_energy(eatom)
                energy+=eatom
                p.set_gradient(gradient)
                p.reset_neigh_distances()
                #print(p.gradient)

        #print("ENERGY {}".format(energy))

        #move
        for p in self.points:
            if p.occupation==0:
                pos=p.pos
                gr=p.gradient_unitary
                new_pos=[pi-gi*self.linmove for pi, gi in zip(pos, gr)]
                #print(pos, new_pos)
                p.move(new_pos, self.pbc)

        converged=False
        if abs(self.minimization_e-energy)/energy < self.tolerance:
            converged=True
            print("CONVERGED AFTER {} STEPS, LINMOVE {}".format(nstep, self.linmove))
            self.minimization_e=100000000000000.0
        if (self.minimization_e>energy) and nstep!=1: #increase the step if we ahve gone down in energy
            #print("ENERGY WENT DOWN OLDE {} NEWE {} INCREASING LINMOVE OLD {} NEW {}".format(self.minimization_e, energy, self.linmove, self.linmove*1.2))
            self.linmove=1.2*self.linmove
        elif (self.minimization_e<energy) and nstep!=1: #decrease it if the energy has increased
            #print("ENERGY WENT UP OLDE {} NEWE {} DECREASING LINMOVE OLD {} NEW {}".format(self.minimization_e, energy, self.linmove, self.linmove*0.5))
            self.linmove=0.5*self.linmove
        self.minimization_e=energy
        return converged            

    def get_gradient(self, potential, central_position, neighbor_positions, distances):
        gradient=[0.0, 0.0, 0.0]
        eatom=0.0
        for npos, dist in zip(neighbor_positions, distances):
            e, gr=potential.der(dist, central_position, npos, self.pbc)
            eatom+=e
            gradient=[g1+g2 for g1, g2 in zip(gr, gradient)]
        return eatom, gradient

    def get_energy(self, potential, central_position, positions):
        e=0.0
        for pos in positions:
            e+=potential.evl(central_position, pos, self.pbc)
        return e

    def output_grid_to_file(self, outfile_name, mode="a"):
        outfile=open(outfile_name, mode=mode)
        outfile.write("{}\n".format(len(self.points)))
        if not(self.is_periodic):
            outfile.write('Lattice="0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0" Properties=ID:I:1:species:S:1:pos:R:3\n')
        else:
            outfile.write('Lattice="{0[0]} 0.0 0.0 0.0 {0[1]} 0.0 0.0 0.0 {0[2]}" Properties=ID:I:1:species:S:1:pos:R:3\n'.format(self.pbc))

        for i, p in enumerate(self.points):
            if p.occupation==0:
                elm="H"
            elif p.occupation==1:
                elm="C"
            else:
                elm="N"
            #better number format so files don't get so huge
            outfile.write("{1} {2} {0[0]: 10.4f} {0[1]: 10.4f} {0[2]: 10.4f}\n".format(p.pos, i+1, elm))
        outfile.close()

class dead_zone_square_well_potential:
    def __init__(self, dead_zone=1.0, spring_constant=0.1, equil_distance=1.0):
        self.dz=dead_zone
        self.k=spring_constant
        self.req=equil_distance

    def evl(self, r1, r2, pbc):
        r=get_distance(r1, r2, pbc)
        if r>(self.req+0.5*self.dz) or r<(self.req-0.5*self.dz):
            return 0.5*self.k*(r-self.req)**2
        else:
            return 0.0

    def der(self, r, r1, r2, pbc):
        #r=get_distance(r1, r2, pbc)
        if r>(self.req+0.5*self.dz) or r<(self.req-0.5*self.dz):
            dri=get_difference_vector(r1, r2, pbc)
            return  0.5*self.k*(r-self.req)**2, [self.k*(r-self.req)*dri[0]/r, 
                    self.k*(r-self.req)*dri[1]/r,
                    self.k*(r-self.req)*dri[2]/r]
        else:
            return 0.0, [0.0, 0.0, 0.0]

class simulation:
    def __init__(self, name="no_name", verbose=False):
        dt     =0.002
        epsilon= 1.0 #0.05
        sigma  = 1.0 #2.55
        cutoff =12.0
        if verbose:
            self.lmp=lammps(cmdargs=["-screen", "none"])
        else:
            self.lmp=lammps(cmdargs=["-screen", "none", "-log", "none"])
        self.lmp.commands_list(["units lj",
                                "boundary m m m",
                                "atom_style atomic",
                                "atom_modify map array",
                                "timestep {}".format(dt)
                                ])
        self.lmp.commands_list(["region my_box block -50.0 50.0 -50.0 50.0 -50.0 50.0", "create_box 1 my_box"])

        self.lmp.commands_list(["pair_style lj/cut {}".format(cutoff),
                                "pair_coeff * * {} {}".format(epsilon, sigma),
                                "mass 1 10.0",
                                "pair_modify tail yes",
                                ])

        self.min_e=100.0
        self.name=name
    
    def finish(self, ):
        self.lmp.close()

    def create_initial_atoms(self, grid_points):
        nid=0
        for gp in grid_points:
            if gp.occupation!=0:
                self.lmp.command("create_atoms 1 single {0[0]} {0[1]} {0[2]} units box".format(gp.pos))
                gp.set_sim_id(nid)
                nid+=1
        self.lmp.command("run 0")
        #self.lmp.command("dump lammpstrj all custom 50 {}.grid.lammpstrj id type x y z".format(self.name))
        #self.lmp.command("dump_modify lammpstrj first no sort id")
        print("WARNING: ADDING FIX RECENTER")
        #self.lmp.command("fix 1 all recenter INIT INIT INIT units box")
        self.lmp.command("minimize 1.0e-6 1.0e-4 1000 100000")

        #move grid points
        post_min_array=self.lmp.gather_atoms("x", 1, 3) #returns a copy of the array, 1D, as x_1, y_1, z_1, x_2, y_2, etc
        for gp in grid_points:
            if gp.occupation!=0 and gp.sim_id>=0:
                new_pos=[post_min_array[3*gp.sim_id+0], post_min_array[3*gp.sim_id+1], post_min_array[3*gp.sim_id+2]]
                #print(gp.sim_id, new_pos)
                gp.move(new_pos, None)

        #self.lmp.create_atoms(natoms, None, elm, pos, vel, shrinkexceed=True)

    def mc_exchange_step(self, grid_points, temp, pbc):
        #store initial conditions, lmp object keeps its state
        pre_pos_array=self.lmp.gather_atoms("x", 1, 3) #returns a copy of the array, 1D, as x_1, y_1, z_1, x_2, y_2, etc
        pre_elm_array=self.lmp.gather_atoms("type", 0, 1) #we need these two for the scattering, even tho they are all 0/irrelevant
        pre_vel_array=self.lmp.gather_atoms("v", 1, 3)
        self.lmp.command("run 0")
        pre_e=self.lmp.extract_compute("thermo_pe",0,0)

        #find empty and full sites, exchange
        occupied=[]  #this could be implemented as functions of the grid itself, saves time recomputing them, but should be fast enough as it is
        empty=[]
        for n, gp in enumerate(grid_points):
            if gp.occupation==0:
                empty.append(n)
            elif gp.occupation==1:
                occupied.append(n)

        oid=sample(occupied, 1)[0]
        eid=sample(empty, 1)[0]
        grid_points[oid].flip()
        grid_points[eid].flip()


        #delete and recreate
        self.lmp.command("delete_atoms group all")
        nid=0
        for gp in grid_points:
            if gp.occupation!=0:
                self.lmp.command("create_atoms 1 single {0[0]} {0[1]} {0[2]} units box".format(gp.pos))
                gp.set_sim_id(nid)
                nid+=1
        #self.lmp.command("run 0")
        self.lmp.command("minimize 1.0e-6 1.0e-4 1000 100000")
        post_e=self.lmp.extract_compute("thermo_pe",0,0)

        #evaluate MC parameter
        prob=min(e**(-1.0*(post_e-pre_e)/(k_b*temp)), 1.0)
        ran =uniform(0.0, 1.0)

        #print(prob, ran, pre_e, post_e)

        #restore or continue
        if ran<prob: #accept, update grid
            post_min_array=self.lmp.gather_atoms("x", 1, 3) #returns a copy of the array, 1D, as x_1, y_1, z_1, x_2, y_2, etc
            for gp in grid_points:
                if gp.occupation!=0 and gp.sim_id>=0:
                    new_pos=[post_min_array[3*gp.sim_id+0], post_min_array[3*gp.sim_id+1], post_min_array[3*gp.sim_id+2]]
                    #print(gp.sim_id, new_pos)
                    gp.move(new_pos, pbc)

            if post_e<self.min_e:
                #print("NEW GLOBAL MINIMUM FOUND: NEW {} OLD {}".format(post_e, self.min_e))
                self.min_e=post_e
                self.lmp.command("write_dump all custom min.{}.lammpstrj id type x y z modify sort id append yes".format(self.name))
            return True, post_e
        else: #reject, restore system
            grid_points[oid].flip()
            grid_points[eid].flip()
            natoms=self.lmp.extract_global("natoms", 0)
            self.lmp.command("delete_atoms group all")
            self.lmp.create_atoms(natoms, None, pre_elm_array, pre_pos_array, pre_vel_array, shrinkexceed=True)
            return False, pre_e

    def dump_coordinates(self, filename):
        self.lmp.command("write_dump all custom {}.{}.lammpstrj id type x y z modify sort id append yes".format(filename, self.name))

        #pass back to grid

    def dump_restart(self, grid_points, pbc, nstep):
        rfile=open("restart.restart", mode="w")

        rfile.write("iter_step {}\n".format(nstep))
        
        for i, pbci in enumerate(pbc):
            rfile.write("pbc {} {}\n".format(i+1, pbci))

        for point in grid_points:
            rfile.write("grid {0[0]} {0[1]} {0[2]} {1}\n".format(point.pos, point.occupation))

        atom_pos=self.lmp.gather_atoms("x", 1, 3)
        natoms=self.lmp.get_natoms()

        for i in range(0, natoms):
            rfile.write("atom {0[0]} {0[1]} {0[2]}\n".format(atom_pos[i*3:(i*3)+3]))

        rfile.close()
        

def nop(*args, **kwargs):
    pass

##################
### CONSTANTS  ###
##################

k_b=1.0  #8.6173303E-5#eV/K

##################
### PARAMETERS ###
##################
verbose=False

#natoms=38 #icosahedral LJ GO
#npoints=8
search_global_minima=True
nmin=50
nreneigh=30
noutgrid=1000
noutmc=1000
temp=0.825 #K
#max_neigh=6 #need to increase this for higher dimensions, more neighbors on a 3D grid than in a 2D one, but also makes it slower

restart=False
restart_file_name="restart.restart"
nrestart=1000

##################
### SED MOD    ###
##################
#5000 steps seems to be a good number up to around 50 atoms, but the algorithms stops once it finds the global opt either way
nsteps=1000
# this is now more of a label for which repetition number this is rather than assigning the number of repetitions to perform
nrepeats_min=1
nrepeats_max=1
lj_min=60
lj_max=60
iseed=485484
seed(iseed)

dim=3
if dim==2:
    npoints_multiplier=50
    max_neigh=6
    grid_initial_distance=0.45
    equil_distance=0.6
    dead_zone=0.2
elif dim==3:
    npoints_multiplier=40
    max_neigh=12
    grid_initial_distance=0.5
    equil_distance=0.6
    dead_zone=0.2

for natoms in range(lj_min, lj_max+1):
    for nrep in range(nrepeats_min, nrepeats_max+1):
        name="{}-{}".format(natoms, nrep)
        grid_file_name="grid.{}.xyz".format(name)

        #retrieve the known GO energy
        go_e=0.0
        if search_global_minima:
            with open("lj_minima_energy.txt", mode="r") as gofile:
                for line in gofile:
                    if line[0]!="#": #skip comment lines
                        spline=line.split()
                        if int(spline[0]) == natoms:
                            go_e=float(spline[2])
                            break

        #calculate required npoints, in total it should be around 10-25-30X the number of LJ particles?
        req_npoints=npoints_multiplier*natoms
        npoints=[ceil(req_npoints**(1/dim))]*dim
        if len(npoints)==2:
            npoints.append(0)
        print(npoints)

        #initialize grid
        my_grid=grid()
    
        if not verbose:
            my_grid.output_grid_to_file=nop

        my_pot=dead_zone_square_well_potential(dead_zone=dead_zone, spring_constant=0.2, equil_distance=equil_distance)
        my_grid.set_potential(my_pot)

        if not restart:
            #initialize atoms within a region
            if dim==2:
                my_grid.start_grid_2D(distance=grid_initial_distance, npoints=npoints)
            elif dim==3:
                my_grid.start_grid_3D(distance=grid_initial_distance, npoints=npoints)

            my_grid.flip_points_in_region(natoms, 
                                        center=[npoints[0]*grid_initial_distance*0.5, npoints[1]*grid_initial_distance*0.5, npoints[2]*grid_initial_distance*0.5], 
                                        radius=(natoms**(1/dim))*grid_initial_distance*1.5)
            if dim==2:
                my_grid.set_pbc(pbc=[(npoints[0]+0)*grid_initial_distance*1.0,
                                 (npoints[1]+0)*grid_initial_distance*1.0,
                                 (npoints[2]+0)*grid_initial_distance+10,
                                ])
            elif dim==3:
                my_grid.set_pbc(pbc=[(npoints[0]+0)*grid_initial_distance*1.0,
                                 (npoints[1]+0)*grid_initial_distance*1.0,
                                 (npoints[2]+0)*grid_initial_distance*1.0,
                                ])
            my_grid.output_grid_to_file(grid_file_name, mode="w")
            my_grid.delete_unoccupied()
            my_grid.output_grid_to_file(grid_file_name, mode="a")

        ninit=0
        if restart:
            ninit=my_grid.read_restart(restart_file_name)

        #create and minimize atoms
        my_sim=simulation(name=name, verbose=verbose)
        my_sim.create_initial_atoms(my_grid.points)
        my_grid.output_grid_to_file(grid_file_name, mode="a")

        #recreate grid around the atoms
        if not restart:
            if dim==2:
                my_grid.start_grid_2D(distance=grid_initial_distance, npoints=npoints)
            elif dim==3:
                my_grid.start_grid_3D(distance=grid_initial_distance, npoints=npoints)

        my_grid.output_grid_to_file(grid_file_name, mode="a")
        my_grid.assign_neighbors(max_neigh=max_neigh)
        #initial grid minimization
        for i in range(1, (nmin)*2+1):
            my_grid.optimize()
            if i%25==0:
                my_grid.assign_neighbors(max_neigh=max_neigh)
                my_grid.output_grid_to_file(grid_file_name, mode="a")


        # loop MC steps and grid minimization

        succ=False
        nsucc=0
        min_e=100.0
        logfile=open("{}.log".format(name), mode="w")

        for nstep in range(ninit+1, ninit+nsteps+1):
            #print("MC STEP {}\n".format(nstep))
            #minimize grid
            if succ:
                #print("MINIMIZING GRID")
                my_grid.output_grid_to_file(grid_file_name, mode="a")
                my_grid.assign_neighbors(max_neigh=max_neigh)
                for i in range(1, nmin+1):
                    converged=my_grid.optimize(nstep=i)
                    if i%nreneigh==0:
                        my_grid.assign_neighbors(max_neigh=max_neigh)
                    if i%noutgrid==0:
                        my_grid.output_grid_to_file(grid_file_name, mode="a")
                    if converged:
                        my_grid.output_grid_to_file(grid_file_name, mode="a")
                        break


            #MC step
            #print("EXCHANGING SITES")
            succ, curr_e=my_sim.mc_exchange_step(my_grid.points, temp, my_grid.pbc)
            if succ:
                nsucc+=1
                my_sim.dump_coordinates("markov") #output accepted structures
            logfile.write("NSTEP {} SUCCESS {} ACCEPTED {} REJECTED {} TOTAL {} ACC.RATIO {} ENERGY {}\n".format(nstep, succ, nsucc, nstep-nsucc, nstep, nsucc/nstep, curr_e))
            if curr_e<min_e:
                logfile.write("NSTEP {} NEW GLOBAL MINIMUM FOUND CURRENT {} OLD {} TARGET {}\n".format(nstep, curr_e, min_e, go_e))
                min_e=curr_e
            if search_global_minima and abs(min_e-go_e)<my_grid.tolerance :
                logfile.write("NSTEP {} TABULATED GLOBAL MINIMA FOUND CURRENT {} TARGET {}".format(nstep, min_e, go_e))
                break
            if nstep%noutmc == 0: #output all structures regularly, for proper MC averaging
                my_sim.dump_coordinates("outmc")

            if nstep%nrestart == 0: #output structure for restarting
                my_sim.dump_restart(my_grid.points, my_grid.pbc, nstep)

        logfile.close()
        my_sim.finish()

#TO DO
# ADD: Normal local MC/MD move to dislodge system
